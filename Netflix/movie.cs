﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace Netflix
{
    public class movie
    {
        private string name;
        public string getName()
        {
            return name;
        }
        public void setName(string name)
        {
            this.name = name;
        }
        private string description;
        public string getDes()
        {
            return this.description;
        }
        public void setDes(string des)
        {
            this.description = des;
        }
        private int longMovie;
        public int getLong()
        {
            return longMovie;
        }
        public void setLong(int longM)
        {
            this.longMovie = longM;
        }
        private int ID;
        public int getID()
        {
            return this.ID;
        }
        public void setID(int ID)
        {
            this.ID = ID;
        }
        private string image;
        public string getImage()
        {
            return this.image;
        }
        public void setImage(string image)
        {
            this.image = image;
        }
        private string releseDate;
        public string getRele()
        {
            return this.releseDate;
        }
        public void setRele(string reles)
        {
            this.releseDate = reles;
        }

        public movie(string name)
        {
            this.name = name;

            string connectionString = DB.connection;

            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT        id, photo, description, longM, releseDate
                                FROM            movies
                                WHERE        (name = '{name}');";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            r.Read();
            object[] vals = new object[5] ;
            r.GetValues(vals);
            this.setID ( (int)vals[0]);
            this.image = vals[1].ToString();
            this.description = vals[2].ToString();
            this.longMovie = (int)vals[3];
            this.releseDate = vals[4].ToString();
            con.Close();

        }
    }
}