﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net;
using System.Net.Mime;
using System.IO;

namespace Netflix
{
    public class Mail
    {
        /*
         * The method sends mail message to a user mail
         * input: the user mail, the message bode, the message subject
         */
        public static void sendMsg(string to, string msg, string sub)
        {
            MailAddress Mto = new MailAddress(to);
            MailAddress from = new MailAddress("netflix.and.me4u@gmail.com");
            MailMessage message = new MailMessage(from, Mto);
            message.Subject = sub;
            string file = @"C:\Users\משתמש\Desktop\netflix_me\Netflix\logo.png";
            string body = $@"<h1>{msg} </h1> <br/>";
            message.Body = body;
            message.IsBodyHtml = true;
            
            Attachment data = new Attachment(file, MediaTypeNames.Application.Octet);
            // Add time stamp information for the file.
            ContentDisposition disposition = data.ContentDisposition;
            disposition.CreationDate = System.IO.File.GetCreationTime(file);
            disposition.ModificationDate = System.IO.File.GetLastWriteTime(file);
            disposition.ReadDate = System.IO.File.GetLastAccessTime(file);
            // Add the file attachment to this email message.
            message.Attachments.Add(data);

            SmtpClient client = new SmtpClient();

            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("netflix.and.me4u@gmail.com", "yovel2003");
            client.Host = "smtp.gmail.com";
            client.Port = 587;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Send(message);

        }
    }
}