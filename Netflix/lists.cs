﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace Netflix
{
    public class lists
    {
        public static bool addSeries(string name, string desc, string photo, string releseDate, int numOfEpisodes)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"INSERT INTO `netflix&me`.`serieses` (name, description, releseDate, numOfEpisodes, photo) 
                                VALUES ('{name}', '{desc}', '{releseDate}', '{numOfEpisodes}','{photo}');";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            int res = cmd.ExecuteNonQuery();
            con.Close();
            if (res == 0)
            {
                return false;
            }
            return true;
        }
        public static bool addMovie(string name, string desc, string photo, string releseDate, int longM)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"INSERT INTO `netflix&me`.`movies` (name, description, releseDate, longM, photo) 
                                VALUES ('{name}', '{desc}', '{releseDate}', '{longM}','{photo}');";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            int res = cmd.ExecuteNonQuery();
            con.Close();
            if (res == 0)
            {
                return false;
            }
            return true;
        }
        public static List<movie> getAllMovies()
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT name FROM `netflix&me`.`movies` ;";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            List<movie> movies = new List<movie>();
            while (r.Read())
            {
                movies.Add(new movie(r.GetValue(0).ToString()));
            }
            con.Close();
            return movies;
        }
        public static List<series> getAllSeries()
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT name FROM `netflix&me`.`serieses` ;";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            List<series> series = new List<series>();
            while (r.Read())
            {
                series.Add(new series(r.GetValue(0).ToString()));
            }
            con.Close();
            return series;
        }
        /*
         The method returns all the serieses two users has in common
             */
        public static List<series> getFriendsMatchSeries(string userID, string friendID)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT serieses.name
                                FROM saveseries
                                INNER JOIN serieses ON saveseries.seriesID=serieses.id
                                WHERE userID='{userID}' AND serieses.name in (
                                SELECT serieses.name
                                FROM saveseries
                                INNER JOIN serieses ON saveseries.seriesID=serieses.id
                                WHERE userID='{friendID}');";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            List<series> series = new List<series>();
            while (r.Read())
            {
                series.Add(new series(r.GetValue(0).ToString()));
            }
            con.Close();
            return series;
        }
        /*
       The method returns all the movies two users has in common
           */
        public static List<movie> getFriendsMatchMovies(string userID, string friendID)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT movies.name
                                FROM savemovies
                                INNER JOIN movies ON savemovies.movieID=movies.id
                                WHERE userID='{userID}' AND movies.name in (
                                SELECT movies.name
                                FROM savemovies
                                INNER JOIN movies ON savemovies.movieID=movies.id
                                WHERE userID='{friendID}');";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            List<movie> movies = new List<movie>();
            while (r.Read())
            {
                movies.Add(new movie(r.GetValue(0).ToString()));
            }
            con.Close();
            return movies;
        }
    }
}