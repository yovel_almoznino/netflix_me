﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;


namespace Netflix
{
    public class statistics
    {
        public static int getViews()
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT views FROM `netflix&me`.statistics where id=1;";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            r.Read();
            int views = int.Parse(r.GetValue(0).ToString());

            return views;
        }
        public static void addViews()
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"update statistics Set views = views + 1 where id=1;";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            int r = cmd.ExecuteNonQuery();
        }

        public static int getUsers()
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT users FROM `netflix&me`.statistics where id=1;";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            r.Read();
            int views = int.Parse(r.GetValue(0).ToString());

            return views;
        }
        public static void addUser()
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"update statistics Set users = users + 1 where id=1;";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            int r = cmd.ExecuteNonQuery();
        }
        public static void removeUser()
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"update statistics Set users = users - 1 where id=1;";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            int r = cmd.ExecuteNonQuery();
        }

        public static int getSeries()
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT sumSeries FROM `netflix&me`.statistics where id=1;";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            r.Read();
            int views = int.Parse(r.GetValue(0).ToString());

            return views;
        }
        public static void addSeries()
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"update statistics Set sumSeries = sumSeries + 1 where id=1;";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            int r = cmd.ExecuteNonQuery();
        }
        public static void removeSeries()
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"update statistics Set sumSeries = sumSeries - 1 where id=1;";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            int r = cmd.ExecuteNonQuery();
        }

        public static int getMovies()
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT sumMovies FROM `netflix&me`.statistics where id=1;";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            r.Read();
            int views = int.Parse(r.GetValue(0).ToString());

            return views;
        }
        public static void addMovie()
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"update statistics Set sumMovies = sumMovies + 1 where id=1;";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            int r = cmd.ExecuteNonQuery();
        }
        public static void removeMovie()
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"update statistics Set sumMovies = sumMovies - 1 where id=1;";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            int r = cmd.ExecuteNonQuery();
        }

    }
}