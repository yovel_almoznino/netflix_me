﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netflix
{
    public partial class addMovie : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void adMovie_Click(object sender, EventArgs e)
        {
            bool r = lists.addMovie(name.Text, description.Text, photo.Text, date.Text, int.Parse(longM.Text));
            res.Text = r.ToString();
        }
    }
}