﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace Netflix
{
    public partial class Add_Friends : System.Web.UI.Page
    {
        protected void btn_Click(object sender, EventArgs e)
        {
            CurrentUser currentUser = (CurrentUser)Session["user"];
            Button b = (Button)sender;
            CurrentUser friend = Users.FindByName(b.CommandArgument);
            friends.addFriend(currentUser.getID(), friend.getID());
            Mail.sendMsg(Users.getMailById(friend.getID()), "You have new friend request waiting for you from:\n" + currentUser.getUsername(), "New friend request");
            Server.Transfer("Add Friends.aspx");

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!handler.IsAvailable(new MySqlConnection(DB.connection)))
            {
                Server.Transfer("Error.aspx");
            }
            if (Session["user"] == null)
            {
                Server.Transfer("Login.aspx");
            }
            CurrentUser currentUser = (CurrentUser)Session["user"];
            username.Text = "Hello " + currentUser.getUsername();
            
                List<string> names = friends.getNonFriends(currentUser.getID(), currentUser.getUsername());
                Names.Text = "";
                foreach (string n in names)
                {
                    Names.Text += n;
                    Names.Text += "<br/>";
                    Button b = new Button();
                    b.Text = "Add";
                    b.ID = "Add" + n;
                    b.Click += btn_Click;
                    b.CommandArgument = n;
                    b.CssClass = "button";
                    PlaceHolder1.Controls.Add(b);
                    Label l = new Label();
                    l.Text = "<br/>";
                    PlaceHolder1.Controls.Add(l);
                }
            
            
        }
        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(DropDownList1.SelectedValue != "Add Friends.aspx")
                Server.Transfer(DropDownList1.SelectedValue);
        }
    }
}