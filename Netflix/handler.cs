﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace Netflix
{
    public static class handler
    {
        public static bool IsAvailable(this MySqlConnection conn)
        {
            try
            {
                conn.Open();
                conn.Close();
            }
            catch (MySqlException)
            {
                return false;
            }

            return true;
        }
    }
}