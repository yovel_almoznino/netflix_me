﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="forum.aspx.cs" Inherits="Netflix.forum" %>

<!DOCTYPE HTML>
<!--
	Colorized by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>Netflix&&Me</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link href='http://fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-panels.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel-noscript.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-desktop.css" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->
	</head>
	<body>

	    <form id="form1" runat="server">

	<!-- Header -->
		<div id="header">
			<div id="logo-wrapper">
				<div class="container">
						
					<!-- Logo -->
						<div id="logo">
							<h1><a href="#">Netflix && Me</a></h1>
							<span>Just for You</span>
						</div>
					
				</div>
			</div>			
			<div class="container">
				<!-- Nav -->
					<nav id="nav">
						<ul>
							<li ><a href="Homepage.aspx">Homepage</a></li>													
							<li ><a href="Add_Movie.aspx">Add Movie</a></li>
							<li ><a href="addSeries.aspx">Add Series</a></li>
							<li><a href="favorits.aspx">Favorites</a></li>
							<li><asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" AutoPostBack="True">
                            <asp:ListItem Selected="True" Value="forum.aspx"> Choose </asp:ListItem>
							<asp:ListItem Value="Friends_Me.aspx"> Friends&Me </asp:ListItem>
							<asp:ListItem Value="ViewFriends.aspx"> View Friends </asp:ListItem>
							<asp:ListItem Value="Add Friends.aspx"> Add Friends </asp:ListItem>
							<asp:ListItem Value="ViewRequests.aspx">View Friends Requests</asp:ListItem>
							</asp:DropDownList></li>
							<li ><a href="Update.aspx">Update Personal Info</a></li>
							<li class="active"><a href="forum.aspx">Forum</a></li>
							<li ><a href="ContactAdmin.aspx">Contact Us</a></li>
							<li ><a href="Login.aspx">Logout</a></li>
						</ul>
					</nav>
			</div>
		</div>
	<!-- Header -->
		
	<!-- Banner -->
		<div id="banner">
			<div class="container">
			</div>
		</div>
	<!-- /Banner -->

	<!-- Main -->
		<div id="main">

			<!-- Main Content -->
			<div class="container">
				<div class="row">
				
					<div class="9u skel-cell-important">
						<section>
							<header>
								<h2>Forum</h2>
							</header>
							<p>
                                <asp:Label ID="username" runat="server" Text="Label"></asp:Label>
                            </p>
                            <p>
                                &nbsp;</p>
                            <p>
                                <asp:Label ID="content" runat="server" Text="Label"></asp:Label>
                            </p>
						</section>
					</div>

					<div id="sidebar1" class="3u">
						<section>
							<header>
								<h2>Post new message</h2>
                                <p>pay attention! In order to delete message you need to contact with the admin!</p>
							</header>
						</section>
						<section>
							<asp:TextBox ID="mess" runat="server" Height="87px" Width="189px" TextMode="MultiLine"></asp:TextBox>
                            <br />
                            <br />
                            <asp:Button ID="post" runat="server" Text="Post" Height="26px" Width="75px" OnClick="post_Click" CssClass="button" />
						</section>
					</div>
				
				</div>
			
			</div>
			<!-- /Main Content -->
			
		</div>
	<!-- /Main -->

	<!-- Footer -->
		<div id="footer">
			<div class="container">
				<div class="row">
					<div class="6u">
						<section>
						    
						</section>
					</div>
				</div>
				
				
			</div>
		</div>
        </form>


	</body>
</html>
