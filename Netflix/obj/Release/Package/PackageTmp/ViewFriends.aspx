﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewFriends.aspx.cs" Inherits="Netflix.ViewFriends" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>


<!DOCTYPE HTML>
<!--
	Colorized by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<head runat="server">
	<title>Netflix&&Me</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link href='https://fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
	<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="js/skel.min.js"></script>
	<script src="js/skel-panels.min.js"></script>
	<script src="js/init.js"></script>
	<noscript>
		<link rel="stylesheet" href="css/skel-noscript.css" />
		<link rel="stylesheet" href="css/style.css" />
		<link rel="stylesheet" href="css/style-desktop.css" />
	</noscript>
	<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
	<!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->
</head>
	<body>
		<form id="form1" runat="server">
			<!-- Header -->
			<div id="header">
				<div id="logo-wrapper">
					<div class="container">

						<!-- Logo -->
						<div id="logo">
							<h1><a href="#">Netflix && Me</a></h1>
							<span>Just for You</span>
						</div>

					</div>
				</div>
				<div class="container">
					<!-- Nav -->
					<nav id="nav">
						<ul>
							<li ><a href="Homepage.aspx">Homepage</a></li>													
							<li ><a href="Add_Movie.aspx">Add Movie</a></li>
							<li ><a href="addSeries.aspx">Add Series</a></li>
							<li><a href="favorits.aspx">Favorites</a></li>
							<li class="active"><asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" AutoPostBack="True">
                            <asp:ListItem  Value="ViewFriends.aspx"> Choose </asp:ListItem>
							<asp:ListItem Value="Friends_Me.aspx"> Friends&Me </asp:ListItem>
							<asp:ListItem Selected="True" Value="ViewFriends.aspx"> View Friends </asp:ListItem>
							<asp:ListItem Value="Add Friends.aspx"> Add Friends </asp:ListItem>
							<asp:ListItem Value="ViewRequests.aspx">View Friends Requests</asp:ListItem>
							</asp:DropDownList></li>
							<li ><a href="Update.aspx">Update Personal Info</a></li>
							<li ><a href="forum.aspx">Forum</a></li>
							<li ><a href="ContactAdmin.aspx">Contact Us</a></li>
							<li ><a href="Login.aspx">Logout</a></li>
						</ul>
					</nav>
				</div>
			</div>
			<!-- Header -->
			<!-- Banner -->
			<div id="banner">
				<div class="container">
				</div>
			</div>
			<!-- /Banner -->
			<!-- Main -->
			<div id="main">

				<!-- Main Content -->
				<div class="container">
					<div class="row">
						<div class="12u">
							<section>
								<header>
									<h2>Welcome!</h2>
								</header>
								<div>
									<asp:Label ID="username" runat="server" Text=""></asp:Label>
									<br /><br /><br />
									<div  style="float: right; width:300px">
										<asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
									</div>
									<div style="float:left; width:200px">

										<asp:Label ID="Names"  runat="server" Font-Bold="True" Font-Size="Large" ></asp:Label>
									</div>
								    
								    
								    
								</div>
							</section>
						</div>
					</div>

				</div>
				<!-- /Main Content -->

			</div>
			<!-- /Main -->
			<!-- Footer -->
			<div id="footer">
				<div class="container">
					<div class="row">
						
						
					</div>


				</div>
			</div>
			<!-- /Footer -->
			<!-- Copyright -->

			</form>
	</body>
</html>

