﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Update.aspx.cs" Inherits="Netflix.Update" %>

<!DOCTYPE HTML>
<!--
	Colorized by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<head runat="server">
	<title>Netflix&&Me</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link href='https://fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
	<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="js/skel.min.js"></script>
	<script src="js/skel-panels.min.js"></script>
	<script src="js/init.js"></script>
	<noscript>
		<link rel="stylesheet" href="css/skel-noscript.css" />
		<link rel="stylesheet" href="css/style.css" />
		<link rel="stylesheet" href="css/style-desktop.css" />
	</noscript>
	<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
	<!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->
</head>
	<body>
		<form id="form1" runat="server">
			<!-- Header -->
			<div id="header">
				<div id="logo-wrapper">
					<div class="container">

						<!-- Logo -->
						<div id="logo">
							<h1><a href="#">Netflix && Me</a></h1>
							<span>Just for You</span>
						</div>

					</div>
				</div>
				<div class="container">
					<!-- Nav -->
					<nav id="nav">
						<ul>
							
							<li  ><a href="Homepage.aspx">Homepage</a></li>													
							<li ><a href="Add_Movie.aspx">Add Movie</a></li>
							<li ><a href="addSeries.aspx">Add Series</a></li>
							<li><a href="favorits.aspx">Favorites</a></li>
							<li><asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" AutoPostBack="True">
                            <asp:ListItem Selected="True" Value="Update.aspx"> Choose </asp:ListItem>
							<asp:ListItem Value="Friends_Me.aspx"> Friends&Me </asp:ListItem>
							<asp:ListItem Value="ViewFriends.aspx"> View Friends </asp:ListItem>
							<asp:ListItem Value="Add Friends.aspx"> Add Friends </asp:ListItem>
							<asp:ListItem Value="ViewRequests.aspx">View Friends Requests</asp:ListItem>
							</asp:DropDownList></li>
							<li class="active"><a href="Update.aspx">Update Personal Info</a></li>
							<li ><a href="forum.aspx">Forum</a></li>
							<li ><a href="ContactAdmin.aspx">Contact Us</a></li>
							<li ><a href="Login.aspx">Logout</a></li>
						</ul>
					</nav>
				</div>
			</div>
			<!-- Header -->
			<!-- Banner -->
			<div id="banner">
				<div class="container">
				</div>
			</div>
			<!-- /Banner -->
			<!-- Main -->
			<div id="main">

				<!-- Main Content -->
				<div class="container">
					<div class="row">
						<div class="12u">
							<section>
								<header>
									<h2>Update Your Personal Info</h2>
                                    <p>&nbsp;</p>
                                    <p>
                                        <asp:Label ID="user" runat="server" Text="Label"></asp:Label>
                                    </p>
								</header>
								<div>
									Username:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="username" runat="server" Width="600" BorderStyle="Solid"></asp:TextBox>
									<br />
									Password:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="password" runat="server" Width="600" BorderStyle="Solid"></asp:TextBox>
									<br />
									Email:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="mail" runat="server" Width="600" BorderStyle="Solid"></asp:TextBox>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<br />
									phone Number:<asp:TextBox ID="phone" runat="server" Width="600" BorderStyle="Solid"></asp:TextBox>
								</div>
								<asp:Button ID="Button1" runat="server" Text="Update!" OnClick="Button1_Click" cssClass="button"/>
								<p>
									<asp:Label ID="Res" runat="server" ForeColor="Red"></asp:Label>
								</p>
							</section>
						</div>
					</div>

				</div>
				<!-- /Main Content -->

			</div>
			<!-- /Main -->
			<!-- Footer -->
			<div id="footer">
				<div class="container">
					<div class="row">
						
						
					</div>


				</div>
			</div>
			<!-- /Footer -->
			<!-- Copyright -->

			</form>
	</body>
</html>