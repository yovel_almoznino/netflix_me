﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminForum.aspx.cs" Inherits="Netflix.AdminForum" %>


<!DOCTYPE HTML>
<!--
	Colorized by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>Netflix&&Me</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link href='http://fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-panels.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel-noscript.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-desktop.css" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->
	</head>
	<body>

	    <form id="form1" runat="server">

	<!-- Header -->
		<div id="header">
			<div id="logo-wrapper">
				<div class="container">
						
					<!-- Logo -->
						<div id="logo">
							<h1><a href="#">Netflix && Me</a></h1>
							<span>Just for You</span>
						</div>
					
				</div>
			</div>			
			<div class="container">
				<!-- Nav -->
					<nav id="nav">
						<ul>
							<li ><a href="DeleteUsers.aspx">Remove Users From System</a></li>													
							<li  ><a href="deleteMS.aspx">Delete Movies and Serieses</a></li>	
							<li ><a href="statistics.aspx">View website statistics</a></li>	
							<li class ="active" ><a href="AdminForum.aspx">Forum</a></li>	
							<li ><a href="AdminLogin.aspx">Logout</a></li>	
						</ul>
					</nav>
			</div>
		</div>
	<!-- Header -->
		
	<!-- Banner -->
		<div id="banner">
			<div class="container">
			</div>
		</div>
	<!-- /Banner -->

	<!-- Main -->
		<div id="main">

			<!-- Main Content -->
			<div class="container">
				<div class="row">
				
					<div class="9u skel-cell-important">
						<section>
							<header>
								<h2>Forum</h2>
							</header>
							<p>
                                <asp:Label ID="username" runat="server" Text="Label"></asp:Label>
                            </p>
                            <p>
                                <div  style="float: right; width:300px">
										<asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
									</div>
                            </p>
                            <p>
                                <asp:Label ID="content" runat="server" Text="Label"></asp:Label>
                            </p>
						</section>
					</div>

					<div id="sidebar1" class="3u">
						<section>
							<header>
								<h2>Post new message</h2>
							</header>
						</section>
						<section>
							<asp:TextBox ID="mess" runat="server" Height="87px" Width="189px" TextMode="MultiLine"></asp:TextBox>
                            <br />
                            <br />
                            <asp:Button ID="post" runat="server" Text="Post" Height="26px" Width="75px" OnClick="post_Click" cssClass="button"/>
						</section>
					</div>
				
				</div>
			
			</div>
			<!-- /Main Content -->
			
		</div>
	<!-- /Main -->

	<!-- Footer -->
		<div id="footer">
			<div class="container">
				<div class="row">
					<div class="6u">
						<section>
						</section>
					</div>
				</div>
				
				
			</div>
		</div>
        </form>


	</body>
</html>

