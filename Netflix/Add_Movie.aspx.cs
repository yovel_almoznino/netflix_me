﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace Netflix
{
    public partial class Add_Movie : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!handler.IsAvailable(new MySqlConnection(DB.connection)))
            {
                Server.Transfer("Error.aspx");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            bool r = lists.addMovie(name.Text, desc.Text, link.Text, rele.Text, int.Parse(Minutes.Text));
            Res.Text = r.ToString();
            name.Text = "";
            desc.Text = "";
            link.Text = "";
            rele.Text = "";
            Minutes.Text = "";
        }
        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(DropDownList1.SelectedValue != "Add_Movie.aspx")
                Server.Transfer(DropDownList1.SelectedValue);
        }
    }
}