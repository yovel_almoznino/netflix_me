﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
namespace Netflix
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!handler.IsAvailable(new MySqlConnection(DB.connection)))
            {
                Server.Transfer("Error.aspx");
            }
            if (Page.IsPostBack)
            {
                string input = password.Text;
                password.TextMode = TextBoxMode.Password;
                password.Attributes.Add("value", input);
            }
            else
            {
                statistics.addViews();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string userName = Username.Text;
            string Password = password.Text;

            CurrentUser res = Users.Login(userName, Password);
            if(res != null)
            {
                Session["user"] = res;
                Server.Transfer("Homepage.aspx");
            }
            else
            {
                Res.Text = "User Does Not Exist";
            }
        }

        //protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        //{
        //    string input = password.Text;
        //    if (CheckBox1.Checked)
        //    {
        //        password.TextMode = TextBoxMode.SingleLine;
        //        password.Text = input;
        //    }
        //    if (CheckBox1.Checked == false)
        //    {
        //        password.TextMode = TextBoxMode.Password;
        //        password.Text = input;
        //    }
        //}

        protected void forgot_Click(object sender, EventArgs e)
        {
            Server.Transfer("Fogot.aspx");
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            string input = password.Text;
            if (ImageButton1.ImageUrl == DB.show)
            {
                ImageButton1.ImageUrl = DB.hide;
                password.TextMode = TextBoxMode.SingleLine;
                password.Text = input;
            }
            else
            {
                ImageButton1.ImageUrl = DB.show;
                password.TextMode = TextBoxMode.Password;
                password.Text = input;
            }
        }
    }
}