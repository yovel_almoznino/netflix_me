﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
namespace Netflix
{
    public partial class statistics1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!handler.IsAvailable(new MySqlConnection(DB.connection)))
            {
                Server.Transfer("Error.aspx");
            }
            if (Session["user"] == null)
            {
                Server.Transfer("AdminLogin.aspx");
            }
            CurrentUser currentUser = (CurrentUser)Session["user"];
            username.Text = "Hello " + currentUser.getUsername();
            views.Text = "The website was viewed: " + statistics.getViews() + " times";
            users.Text = statistics.getUsers().ToString() + " Users had registered to the website";
            movies.Text = "The website has " + statistics.getMovies() + " movies";
            series.Text = "The website has " + statistics.getSeries() + " serieses";
        }
    }
}