﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace Netflix
{
    public class Forum
    {
        //The method returns all of the messages in the forum
        public static List<message> GetMessages()
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT author, message FROM `netflix&me`.`forum` ;";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            List<message> msgs = new List<message>();
            while (r.Read())
            {
                msgs.Add(new message(r.GetValue(0).ToString(), r.GetValue(1).ToString()));
            }
            con.Close();
            return msgs;
        }
        public static void postMessage(string author, string message)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"INSERT INTO `netflix&me`.`forum` (author, message) 
                                VALUES ('{author}', '{message}');";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            int res = cmd.ExecuteNonQuery();
            con.Close();
        }
        public static void DeleteMessage(string author, string message)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"delete from forum where (author='{author}' AND message='{message}');";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            int res = cmd.ExecuteNonQuery();
            con.Close();
        }
    }
}