﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
namespace Netflix
{
    public partial class deleteMS : System.Web.UI.Page
    {
        /*
         * The method deletes the selected movie or series.
         * The method decides if the item is movie or series by the command name button attribute.
         * The method takes the item name from the command argument button attribute.
         */
        protected void del_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.CommandName == "movie")
            {
                Admins.deleteMovie(b.CommandArgument);
                statistics.removeMovie();
            }
            if (b.CommandName == "series")
            {
                Admins.deleteSeries(b.CommandArgument);
                statistics.removeSeries();
            }
            Server.Transfer("deleteMS.aspx");
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!handler.IsAvailable(new MySqlConnection(DB.connection)))
            {
                Server.Transfer("Error.aspx");
            }
            if (Session["user"] == null)
            {
                Server.Transfer("AdminLogin.aspx");
            }
            CurrentUser currentUser = (CurrentUser)Session["user"];
            username.Text = "Hello " + currentUser.getUsername();
            content.Text = "";
            photos.Text = "";
            List<movie> m = lists.getAllMovies();
            List<series> s = lists.getAllSeries();
            foreach (movie movies in m)
            {
                content.Text += "<br/>About the movie:<br/>Description:" + movies.getDes() + "<br/>" + "Minutes long:" + movies.getLong() + "<br/>" + "Relesed on:" + movies.getRele() + " <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";
                Button btn = new Button();
                btn.Text = "Delete!!!";
                btn.ID = "del" + movies.getName();
                btn.Click += del_Click;
                btn.CommandName = "movie";
                btn.CommandArgument = movies.getName();
                btn.CssClass = "button";
                Label l1 = new Label();
                l1.Text = " <br/><br/><br/><br/><br/><br/><br/>";
                PlaceHolder1.Controls.Add(l1);
                PlaceHolder1.Controls.Add(btn);
                Label l = new Label();
                l.Text = " <br/><br/><br/><br/><br/><br/><br/><br/><br/>";
                PlaceHolder1.Controls.Add(l);
                photos.Text += movies.getName() + " <br/>  " + "<img src=" + movies.getImage() + " height='300' width='280'>" + "<br/><br/><br/>";
            }
            foreach (series series in s)
            {
                content.Text += "<br/>About the series:<br/>Description:" + series.getDes() + "<br/>" + "Num of episodes:" + series.getLong() + "<br/>" + "Relesed on:" + series.getRele() + " <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";
                Button btn = new Button();
                btn.Text = "Delete!!";
                btn.ID = "del" + series.getName();
                btn.Click += del_Click;
                btn.CommandName = "series";
                btn.CommandArgument = series.getName();
                btn.CssClass = "button";
                Label l1 = new Label();
                l1.Text = " <br/><br/><br/><br/><br/><br/><br/>";
                PlaceHolder1.Controls.Add(l1);
                PlaceHolder1.Controls.Add(btn);
                Label l = new Label();
                l.Text = "<br/><br/><br/><br/><br/><br/><br/><br/><br/>";
                PlaceHolder1.Controls.Add(l);
                photos.Text += series.getName() + " <br/>  " + "<img src=" + series.getImage() + " height='300' width='280'>" + "<br/><br/><br/>";

            }
        }
    }
}