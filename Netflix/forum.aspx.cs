﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace Netflix
{
    public partial class forum : System.Web.UI.Page
    {
        /*
         * The method deletes the selected message from the forum
         */
        protected void btn_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            CurrentUser user = (CurrentUser)Session["user"];
            Forum.DeleteMessage(user.getUsername(), b.CommandName);
            Server.Transfer("forum.aspx");

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!handler.IsAvailable(new MySqlConnection(DB.connection)))
            {
                Server.Transfer("Error.aspx");
            }
            if (Session["user"] == null)
            {
                Server.Transfer("Login.aspx");
            }
            CurrentUser currentUser = (CurrentUser)Session["user"];
            username.Text = "Hello " + currentUser.getUsername();
            content.Text = "";
            int i = 0;
            List<message> msg = Forum.GetMessages();
            foreach(message m in msg)
            {
                content.Text += m.getAuthor() + ": " + m.getMsg() + "<br/><br/>";
                if (m.getAuthor() == currentUser.getUsername())
                {
                    Button btn = new Button();
                    btn.Text = "Delete!!!";
                    btn.ID = "del" + m.getMsg();
                    btn.Click += btn_Click;
                    btn.CommandName = m.getMsg();
                    btn.CssClass = "button";
                    btn.Font.Size = System.Web.UI.WebControls.FontUnit.Point(9);
                    btn.Height = System.Web.UI.WebControls.Unit.Pixel(20);
                    
                    PlaceHolder1.Controls.Add(btn);
                }
                if (m.getAuthor() != "Admin")
                {
                    CurrentUser user = Users.FindByName((m.getAuthor()));
                    Image profile = new Image();
                    profile.ImageUrl = "data:image;base64," + Convert.ToBase64String(Users.getImageById(user.getID()));
                    profile.Height = System.Web.UI.WebControls.Unit.Pixel(15);
                    profile.Width = System.Web.UI.WebControls.Unit.Pixel(40);
                    profile.ID = "pro" + m.getAuthor();
                    PlaceHolder2.Controls.Add(profile);
                }
               
                Label l1 = new Label();
                l1.Text = " <br/><br/>";
                Label l2 = new Label();
                l2.Text = " <br/><br/>";
                PlaceHolder1.Controls.Add(l1);
                PlaceHolder2.Controls.Add(l2);


            }


        }

        protected void post_Click(object sender, EventArgs e)
        {
            CurrentUser currentUser = (CurrentUser)Session["user"];
            Forum.postMessage(currentUser.getUsername(), mess.Text);
            Server.Transfer("forum.aspx");
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList1.SelectedValue != "forum.aspx")
                Server.Transfer(DropDownList1.SelectedValue);
        }
    }
}