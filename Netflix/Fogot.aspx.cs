﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Twilio;
using Twilio.Types;
using Twilio.Rest.Api.V2010.Account;
using MySql.Data.MySqlClient;

namespace Netflix
{
    public partial class Fogot : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!handler.IsAvailable(new MySqlConnection(DB.connection)))
            {
                Server.Transfer("Error.aspx");
            }
            instructions.Text = "In order to get your password on whatsapp follow this link first!";
            Button2.Enabled = false;

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string mail = Users.getMailById(id.Text);
            string password = Membership.GeneratePassword(7, 3);
            Users.ChangePass(id.Text, password);
            string msg = "Your Password is:\n" + password;
            Mail.sendMsg(mail, msg, "Password Recovery");
            Server.Transfer("Login.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Phone.sendPass(id.Text);
            Server.Transfer("Login.aspx");

        }

        protected void instructions_Click(object sender, EventArgs e)
        {
            Response.Write("<script>");
            Response.Write("window.open('https://api.whatsapp.com/send?phone=+14155238886&text=join into-though','_blank')");
            Response.Write("</script>");
            Button2.Enabled = true;
        }
    }
}