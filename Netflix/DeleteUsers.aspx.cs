﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
namespace Netflix
{
    public partial class DeleteUsers : System.Web.UI.Page
    {
        /*
         * The method deletes the selected user.
         * The method takes the username from the command argument button attribute.
         */
        protected void btn_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            CurrentUser user = Users.FindByName(b.CommandArgument);
            Mail.sendMsg(Users.getMailById(user.getID()), "You were removed from the system :(", "Notice");
            Admins.deleteUser(user.getUsername(),user.getID(),user.getPass());
            statistics.removeUser();
            Server.Transfer("DeleteUsers.aspx");

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!handler.IsAvailable(new MySqlConnection(DB.connection)))
            {
                Server.Transfer("Error.aspx");
            }
            if (Session["user"] == null)
            {
                Server.Transfer("AdminLogin.aspx");
            }
            CurrentUser currentUser = (CurrentUser)Session["user"];
            username.Text = "Hello " + currentUser.getUsername();
            List<string> names = friends.getAllUsers("0");
            Names.Text = "";
            foreach (string n in names)
            {
                Names.Text += n;
                Names.Text += "<br/>";
                Button b = new Button();
                b.CssClass = "button";
                b.Text = "Delete!!!";
                b.ID = "Delete" + n;
                b.Click += btn_Click;
                b.CommandArgument = n;
                PlaceHolder1.Controls.Add(b);
                Label l = new Label();
                l.Text = "<br/>";
                PlaceHolder1.Controls.Add(l);
            }
        }
    }
}