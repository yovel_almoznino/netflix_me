﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
namespace Netflix
{
    public partial class favorits : System.Web.UI.Page
    {
        /*
         * The method deletes the like from the selected movie or series.
         * The method decides if the item is movie or series by the command name button attribute.
         * The method takes the item name from the command argument button attribute.
         */
        protected void unlikes_Click(object sender, EventArgs e)
        {
            CurrentUser currentUser = (CurrentUser)Session["user"];
            Button b = (Button)sender;
            if (b.CommandName == "movie")
            {
                likes.deleteLikeMovies(b.CommandArgument, currentUser.getID());
            }
            if (b.CommandName == "series")
            {
                likes.deleteLikeSeries(b.CommandArgument, currentUser.getID());
            }
            Server.Transfer("favorits.aspx");
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!handler.IsAvailable(new MySqlConnection(DB.connection)))
            {
                Server.Transfer("Error.aspx");
            }
            if (Session["user"] == null)
            {
                Server.Transfer("Login.aspx");
            }
            CurrentUser currentUser = (CurrentUser)Session["user"];
            username.Text = "Hello " + currentUser.getUsername();
            content.Text = "";
            photos.Text = "";
            List<movie> m = Favorits.GetMovies(currentUser.getID());
            List<series> s = Favorits.GetSeries(currentUser.getID());
            foreach (movie movies in m)
            {
                content.Text += "<br/>About the movie:<br/>Description:" + movies.getDes() + "<br/>" + "Minutes long:" + movies.getLong() + "<br/>" + "Relesed on:" + movies.getRele() + " <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";
                Button btn1 = new Button();
                btn1.Text = "unlike!💔";
                btn1.ID = "unlikes" + movies.getName();
                btn1.Click += unlikes_Click;
                btn1.CommandName = "movie";
                btn1.CommandArgument = movies.getName();
                Label l1 = new Label();
                l1.Text = " <br/><br/><br/><br/><br/><br/><br/>";
                PlaceHolder1.Controls.Add(l1);
                btn1.CssClass = "button";
                PlaceHolder1.Controls.Add(btn1);
                Label l = new Label();
                l.Text = " <br/><br/><br/><br/><br/><br/><br/><br/><br/>";
                PlaceHolder1.Controls.Add(l);
                photos.Text += movies.getName() + " <br/>  " + "<img src=" + movies.getImage() + " height='300' width='280'>" + "<br/><br/><br/>";
            }
            foreach (series series in s)
            {
                content.Text += "<br/>About the series:<br/>Description:" + series.getDes() + "<br/>" + "Num of episodes:" + series.getLong() + "<br/>" + "Relesed on:" + series.getRele() + " <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";
                Button btn1 = new Button();
                btn1.Text = "unlike!💔";
                btn1.ID = "unlikes" + series.getName();
                btn1.Click += unlikes_Click;
                btn1.CommandName = "series";
                btn1.CommandArgument = series.getName();
                btn1.CssClass = "button";
                Label l1 = new Label();
                l1.Text = " <br/><br/><br/><br/><br/><br/><br/>";
                PlaceHolder1.Controls.Add(l1);
                PlaceHolder1.Controls.Add(btn1);
                Label l = new Label();
                l.Text = "<br/><br/><br/><br/><br/><br/><br/><br/><br/>";
                PlaceHolder1.Controls.Add(l);
                photos.Text += series.getName() + " <br/>  " + "<img src=" + series.getImage() + " height='300' width='280'>" + "<br/><br/><br/>";

            }




        }
        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList1.SelectedValue != "favorits.aspx")
                Server.Transfer(DropDownList1.SelectedValue);
        }
    }
}