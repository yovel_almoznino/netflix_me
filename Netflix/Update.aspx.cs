﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
namespace Netflix
{
    public partial class Update : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!handler.IsAvailable(new MySqlConnection(DB.connection)))
            {
                Server.Transfer("Error.aspx");
            }
            if (Session["user"] == null)
            {
                Server.Transfer("Login.aspx");
            }
            CurrentUser currentUser = (CurrentUser)Session["user"];
            user.Text = "Hello " + currentUser.getUsername();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            CurrentUser currentUser = (CurrentUser)Session["user"];
            string userName = username.Text;
            string pass = password.Text;
            string num = phone.Text;
            string email = mail.Text;

            if(userName != "")
            {
                currentUser = Users.updateUsername(currentUser.getID(), currentUser.getPass(), userName);
            }
            if(pass != "")
            {
                currentUser = Users.updatePass(currentUser.getID(), pass, currentUser.getUsername());
            }
            if(num != "")
            {
                Users.updatePhoneNum(currentUser.getID(), num);
            }
            if(email != "")
            {
                Users.updateMail(currentUser.getID(), email);
            }
            Session["user"] = currentUser;
            Res.Text = "Updated!";
            username.Text = "";
            password.Text = "";
            phone.Text = "";
            mail.Text = "";

        }
        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList1.SelectedValue != "Upadate.aspx")
                Server.Transfer(DropDownList1.SelectedValue);
        }
    }
}