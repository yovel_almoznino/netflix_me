﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;


namespace Netflix
{
    public partial class SignUp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!handler.IsAvailable(new MySqlConnection(DB.connection)))
            {
                Server.Transfer("Error.aspx");
            }
            if (Page.IsPostBack)
            {
                string input = password.Text;
                password.TextMode = TextBoxMode.Password;
                password.Attributes.Add("value", input);
            }
            else
            {
                statistics.addViews();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            CurrentUser res = Users.SignUP(username.Text, id.Text, password.Text, mail.Text, date.Text, phone.Text);
            
            if (res != null)
            {
                Res.Text = res.ToString();
                Session["user"] = res;
                Mail.sendMsg(Users.getMailById(res.getID()), "Thank you for your subscription to Netflix&&Me", "Welcome");
                statistics.addUser();
                Server.Transfer("Homepage.aspx");
            }
            else
            {
                Res.Text = "Can't Create User";
            }
        }

       

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            string input = password.Text;
            if (ImageButton1.ImageUrl == DB.show)
            {
                ImageButton1.ImageUrl = DB.hide;
                password.TextMode = TextBoxMode.SingleLine;
                password.Text = input;
            }
            else
            {
                ImageButton1.ImageUrl = DB.show;
                password.TextMode = TextBoxMode.Password;
                password.Text = input;
            }
        }
    }
}