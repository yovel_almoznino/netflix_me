﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace Netflix
{
    public partial class Begin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!handler.IsAvailable(new MySqlConnection(DB.connection)))
            {
                Server.Transfer("Error.aspx");
            }
            photos.Controls.Clear();
            List<movie> m = lists.getAllMovies();
            List<series> s = lists.getAllSeries();
            int i = 0;
            foreach (movie movies in m)
            {
                Image im = new Image();
                im.ImageUrl = movies.getImage();
                im.Height = Unit.Pixel(500);
                im.Width = Unit.Pixel(300);
                photos.Controls.Add(im);
                Label space = new Label();
                space.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                photos.Controls.Add(space);
                i++;
                if (i == 3)
                {
                    Label l = new Label();
                    l.Text = "<br/>";
                    photos.Controls.Add(l);
                    i = 0;
                }
            }
            foreach (series series in s)
            {
                Image im = new Image();
                im.ImageUrl = series.getImage();
                im.Height = Unit.Pixel(500);
                im.Width = Unit.Pixel(300);
                photos.Controls.Add(im);
                Label space = new Label();
                space.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                photos.Controls.Add(space);
                i++;
                if (i == 3)
                {
                    Label l = new Label();
                    l.Text = "<br/>";
                    photos.Controls.Add(l);
                    i = 0;
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Server.Transfer("Login.aspx");
        }
    }
}