﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;


namespace WebService
{
    /// <summary>
    /// Summary description for Links
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Links : System.Web.Services.WebService
    {
        
        [WebMethod]
        public List<string> names()
        {

            return link.names();
        }
        [WebMethod]
        public string getLink(string name)
        {
            return link.getLink(name);
        }
        [WebMethod]
        public string getBrowse(string name)
        {
            return link.getBrowse(name);
        }
    }
}
