CREATE DATABASE  IF NOT EXISTS `links` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `links`;
-- MySQL dump 10.13  Distrib 5.7.23, for Win64 (x86_64)
--
-- Host: localhost    Database: links
-- ------------------------------------------------------
-- Server version	5.7.23-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `name_link`
--

DROP TABLE IF EXISTS `name_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `name_link` (
  `name` varchar(45) NOT NULL,
  `link` longtext NOT NULL,
  `browse` longtext NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `name_link`
--

LOCK TABLES `name_link` WRITE;
/*!40000 ALTER TABLE `name_link` DISABLE KEYS */;
INSERT INTO `name_link` VALUES ('Kissing Booth','https://www.youtube.com/watch?v=7bfS6seiLhk&list=PLV5EmtsdcmQd_AYNJGYIBAszVG2Hzxbu2&index=2','https://www.netflix.com/search?q=Kis&jbv=80143556'),('Lucifer','https://www.youtube.com/watch?v=X4bF_quwNtw&list=PLV5EmtsdcmQd_AYNJGYIBAszVG2Hzxbu2&index=5','https://www.netflix.com/search?q=Lu&jbv=80057918'),('Modern Family','https://www.youtube.com/watch?v=q3xrdZpMdu0&list=PLV5EmtsdcmQd_AYNJGYIBAszVG2Hzxbu2&index=3','https://www.netflix.com/search?q=Mod&jbv=70143858'),('New Girl','https://www.youtube.com/watch?v=aW3LEVGQ-lE&list=PLV5EmtsdcmQd_AYNJGYIBAszVG2Hzxbu2&index=4','https://www.netflix.com/search?q=New&jbv=70196145'),('Tall Girl','https://youtu.be/NfpXeLVzJIw?list=PLV5EmtsdcmQd_AYNJGYIBAszVG2Hzxbu2','https://www.netflix.com/search?q=Ta&jbv=81002412');
/*!40000 ALTER TABLE `name_link` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-27  0:34:40
